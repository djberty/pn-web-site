const path = require('path');
module.exports = {
    mode: 'production',
    entry: './src/js/main.js',
    output: {
        path: path.resolve(__dirname, 'web/dist/js'),
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/i,
                include: path.resolve(__dirname, 'src/js'),
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                },
            },
            {
                test: /\.css$/i,
                include: path.resolve(__dirname, 'src/css'),
                use: ['style-loader', 'css-loader', 'postcss-loader'],
            },
        ],
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'web'),
        watchContentBase: true,
    },
};